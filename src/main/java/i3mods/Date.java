package i3mods;

import i3status.Datas;
import i3status.Module;

import java.awt.*;
import java.text.DateFormat;
import java.time.Instant;
import java.util.Calendar;

/**
 * Created by stalai_a on 4/11/17.
 */
public class Date extends Module {
    @Override
    public String name() {
        return "Date";
    }

    @Override
    public void run() {
        d = new Datas();
        DateFormat df = DateFormat.getDateTimeInstance();
        while (true)
        {
            java.util.Date now =  java.util.Date.from(Instant.now());
            Calendar c = Calendar.getInstance();
            java.util.Date date = new java.util.Date();
            d.fulltext = df.format(date);
            /*
            d.fulltext = String.format("%2d/%2d/%d %2d:%2d:%2d",
                    c.get(Calendar.DAY_OF_MONTH),
                    c.get(Calendar.MONTH) + 1,
                    c.get(Calendar.YEAR),
                    c.get(Calendar.HOUR_OF_DAY),
                    c.get(Calendar.MINUTE),
                    c.get(Calendar.SECOND));
                    */

            output(d);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected Datas d;
}
