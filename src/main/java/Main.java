import i3mods.Date;
import i3status.I3status;

/**
 * Created by stalai_a on 4/11/17.
 */
public class Main {
    public static void main(String... Args)
    {
        I3status s = new I3status();
        s.addmodule(new Date());
        s.start();
    }
}
