package i3status;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stalai_a on 4/11/17.
 */
public abstract class Module implements Runnable {
    public abstract String name();
    private final static Map<String, String> cache = new HashMap<>();

    /**
     * Update the current output to s
     * @param s the desired output
     */
    protected final void output(Datas s)
    {
        synchronized (cache)
        {
            cache.put(name(), s.makejson(name()));
            UpdateOutput();
        }
    }

    /**
     * Output the current state of all the modules
     */
    private static void UpdateOutput()
    {
        System.out.print(",[{\"full_text\":\"\"}");//empty element for the ','
        for (String s :cache.values() ) {
            System.out.print(",");
            System.out.print(s);
        }
        System.out.println("]");
    }
}
