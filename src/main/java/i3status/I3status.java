package i3status;

import java.util.ArrayList;

/**
 * Created by stalai_a on 4/11/17.
 */
public class I3status {
    public I3status()
    {
        mods_ = new ArrayList<Module>();
        threads = new ArrayList<>();
    }

    public void addmodule(Module m)
    {
        mods_.add(m);
    }

    public void start() {

        System.out.println("{ \"version\": 1 }");
        System.out.println("[");
        System.out.println("[]");
        for (Module m : mods_ ) {
            Thread t = new Thread(m);
            t.start();
            threads.add(t);
        }
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException ignored) {
            }
        }
    }

    private ArrayList<Module> mods_;
    private ArrayList<Thread> threads;
}
