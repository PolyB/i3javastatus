package i3status;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by stalai_a on 4/12/17.
 * This class helps to generate valid i3-bar output
 */
public class Datas implements Cloneable{
    /**
     * The raw text printed on the bar
     */
    public String fulltext;
    /**
     * The text color
     */
    public Color color;

    public Color background;

    /**
     * Makes the full_text displayed in pango markup
     * (allow color only some part and other fancy stuffs)
     */
    public boolean pango;

    private static String toJson(Map<String, String> s)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        s.forEach((key, val) -> sb.append(String.format("\"%s\":\"%s\",", key, val)));
        sb.deleteCharAt(sb.length() - 1); // remove the last ','
        sb.append("}");
        return sb.toString();
    }

    public String makejson(String name)
    {
        Map<String, String> m = new HashMap<>();
        if (color != null) {
            m.put("color", String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
        }
        if (background != null) {
            m.put("background", String.format("#%02x%02x%02x", background.getRed(), background.getGreen(), background.getBlue()));
        }
        if (pango) {
            m.put("markup", "pango");
        }

        if (fulltext == null)
            m.put("full_text", "");
        else
            m.put("full_text", fulltext);
        return toJson(m);
    }
}
